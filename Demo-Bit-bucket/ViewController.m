//
//  ViewController.m
//  Demo-Bit-bucket
//
//  Created by Saad on 30/5/18.
//  Copyright © 2018 Saad. All rights reserved.
//

#import "ViewController.h"
#import "SubViewController.h"
#import "AppDelegate.h"
#import "QBImagePickerController.h"

#define MAIN_SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)


@interface ViewController (){
    IBOutlet UIButton *DumpingButton;
    CGFloat first;
    UIPanGestureRecognizer *pangeszoom;
    BOOL value;
}

@property (nonatomic,strong) QBImagePickerController *subVc;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    DumpingButton.translatesAutoresizingMaskIntoConstraints = YES;
    DumpingButton.frame=CGRectMake( MAIN_SCREEN_WIDTH/2-22,  SCREEN_HEIGHT-100,  DumpingButton.frame.size.width,  DumpingButton.frame.size.width);
    CGRect frameBtnDown=DumpingButton.frame;
    
    [UIView animateKeyframesWithDuration:1.0 delay:0.0 options:UIViewKeyframeAnimationOptionRepeat | UIViewKeyframeAnimationOptionRepeat|UIViewAnimationOptionAllowUserInteraction animations:^{
        [UIView addKeyframeWithRelativeStartTime:0.0 relativeDuration:0.5 animations:^{
            //DumpingButton.transform=CGAffineTransformMakeScale(1.5, 1.5);
            
            // [self addShadow];
            DumpingButton.frame=CGRectMake(frameBtnDown.origin.x, frameBtnDown.origin.y+10, frameBtnDown.size.width, frameBtnDown.size.width);
            //             DumpingButton.frame=CGRectMake(DumpingButton.frame.origin.x, DumpingButton.frame.origin.y+10, DumpingButton.frame.size.width, DumpingButton.frame.size.width);
            
            
            
        }];
        [UIView addKeyframeWithRelativeStartTime:0.5 relativeDuration:0.5 animations:^{
            DumpingButton.frame=CGRectMake(frameBtnDown.origin.x, frameBtnDown.origin.y, frameBtnDown.size.width, frameBtnDown.size.width);
            //             DumpingButton.frame=CGRectMake(DumpingButton.frame.origin.x, DumpingButton.frame.origin.y-10, DumpingButton.frame.size.width, DumpingButton.frame.size.width);
            DumpingButton.transform=CGAffineTransformIdentity;
            //[self addShadow];
        }];
    } completion:nil];
    
    [self performSelector:@selector(addsubViewController) withObject:nil afterDelay:0.1];

}

-(void)viewWillAppear:(BOOL)animated{
    //[self addsubViewController];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)addsubViewController{
    
    
    
//    self.subVc = [[SubViewController alloc]initWithNibName:@"SubViewController" bundle:nil];
//
//    self.subVc.view.frame = CGRectMake(0, SCREEN_HEIGHT-218, MAIN_SCREEN_WIDTH, SCREEN_HEIGHT);
//    [self.subVc.cancelButton addTarget:self action:@selector(dismissView) forControlEvents:UIControlEventTouchUpInside];
//    self.subVc.cancelButton.hidden = YES;
//    [self addChildViewController:self.subVc];
//    [self.view insertSubview:self.subVc.view belowSubview:DumpingButton];
//    pangeszoom = [[UIPanGestureRecognizer alloc]initWithTarget:self action:@selector(detectPan:)];
//    [self.subVc.view addGestureRecognizer:pangeszoom];
    
    self.subVc = [QBImagePickerController new];
    self.subVc.view.frame = CGRectMake(0, SCREEN_HEIGHT-218, MAIN_SCREEN_WIDTH, SCREEN_HEIGHT);
    //self.subVc.delegate = self;
    self.subVc.allowsMultipleSelection = YES;
    self.subVc.maximumNumberOfSelection = 6;
    self.subVc.showsNumberOfSelectedAssets = YES;
    [self addChildViewController:self.subVc];
    [self.view insertSubview:self.subVc.view belowSubview:DumpingButton];
    pangeszoom = [[UIPanGestureRecognizer alloc]initWithTarget:self action:@selector(detectPan:)];
    [self.subVc.view addGestureRecognizer:pangeszoom];
}

-(void)dismissView{
  
    //self.subVc.cancelButton.hidden = YES;
    DumpingButton.hidden=NO;
    
    [UIView animateWithDuration:0.15 animations:^{
        self.subVc.view.frame = CGRectMake(0, SCREEN_HEIGHT-218, MAIN_SCREEN_WIDTH, SCREEN_HEIGHT);
    } completion:^(BOOL finished) {
        
    }];
}
- (IBAction)presentFullVc:(id)sender {
    //self.subVc.cancelButton.hidden = NO;
    DumpingButton.hidden=YES;
   
    [UIView animateWithDuration:0.15 animations:^{
        self.subVc.view.frame = CGRectMake(0, 0, MAIN_SCREEN_WIDTH, self.subVc.view.frame.size.height);
    } completion:^(BOOL finished) {
        
    }];
}

-(void)detectPan:(UIPanGestureRecognizer *)gestureRecognizer
{
    
    
    
    UIView *anview = self.subVc.view;
    CGPoint translation = [gestureRecognizer translationInView:[anview superview]];
    
    
    if(gestureRecognizer.state == UIGestureRecognizerStateBegan)
    {
        
        first= self.subVc.view.frame.origin.y;
        
        CATransition *animation = [CATransition animation];
        animation.type = kCATransitionFade;
        animation.duration = 0.1;
        //[_navBar.layer addAnimation:animation forKey:nil];
        [DumpingButton.layer addAnimation:animation forKey:nil];
        DumpingButton.hidden=YES;
        
        
        //        self.pageController.view.transform = (self.pageController.view.transform, 1.1, 1.1)
        
    }
    
    
    else if (gestureRecognizer.state == UIGestureRecognizerStateChanged) {
        ;
        
        CGPoint newCenter = CGPointMake(anview.center.x, anview.center.y+translation.y);
        
        anview.center = newCenter;
        
        
        //        [[NSNotificationCenter defaultCenter] postNotificationName:@"ScaleNotification" object:nil];
        
        
        
        //        self.pageController.view.transform = CGAffineTransformScale(self.pageController.view.transform, 1.1, 1.1);
        
        //        gestureRecognizer.s = 1;
        
        
    }
    
    else if (gestureRecognizer.state == UIGestureRecognizerStateEnded)
    {
        
        //self.pageController.view.transform = CGAffineTransformScale(self.pageController.view.transform, 0, 0);
        
        if (first-self.subVc.view.frame.origin.y>30) {
            
            value=YES;
            [self setNeedsStatusBarAppearanceUpdate];
            [UIView animateWithDuration:0.2
                                  delay:0.0
                                options: UIViewAnimationOptionCurveEaseOut
                             animations:^{
                                 
                                 self.subVc.view.frame = CGRectMake(0, 0,  DELEGATE.window.frame.size.width, DELEGATE.window.frame.size.height);
                                 self.subVc.view.layer.cornerRadius = 0;
                                 self.subVc.view.layer.masksToBounds = true;
                                 
                                 // naveviewSecond.hidden=NO;
                             }
                             completion:^(BOOL finished){
                                 
                                 [self.subVc.view removeGestureRecognizer:pangeszoom];
                                 //self.subVc.cancelButton.hidden = NO;
                                 //_navBar.hidden=YES;
                             }];
            
            CATransition *animation = [CATransition animation];
            animation.type = kCATransitionFade;
            animation.duration = 0.2;
           // [_navBar.layer addAnimation:animation forKey:nil];
            [DumpingButton.layer addAnimation:animation forKey:nil];
            
            DumpingButton.hidden=YES;
           // _navBar.hidden = YES;
            
        }
        
        
        else{
            
            
            [UIView animateWithDuration:0.2
                                  delay:0.0
                                options: UIViewAnimationOptionCurveEaseOut
                             animations:^{
                                 
                                 value=NO;
                                 [self setNeedsStatusBarAppearanceUpdate];
                                 
                                 self.subVc.view.frame = CGRectMake(0, first,  DELEGATE.window.frame.size.width, DELEGATE.window.frame.size.height);
                                 
                                 self.subVc.view.layer.cornerRadius = 11;
                                 self.subVc.view.layer.masksToBounds = true;
                                 
                                 //naveviewSecond.hidden=YES;
                             }
                             completion:^(BOOL finished){
                                 
                                 //                   _navBar.hidden=NO;
                                 
                             }];
            
            CATransition *animation = [CATransition animation];
            animation.type = kCATransitionFade;
            animation.duration = 0.2;
            [DumpingButton.layer addAnimation:animation forKey:nil];
            
            DumpingButton.hidden=NO;
            
            //[self DumpingButtonAnimation];
            
            
        }
        
    }
    
    [gestureRecognizer setTranslation:CGPointZero inView:[anview superview]];
}
@end
