//
//  main.m
//  Demo-Bit-bucket
//
//  Created by Saad on 30/5/18.
//  Copyright © 2018 Saad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
