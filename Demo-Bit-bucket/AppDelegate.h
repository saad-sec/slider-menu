//
//  AppDelegate.h
//  Demo-Bit-bucket
//
//  Created by Saad on 30/5/18.
//  Copyright © 2018 Saad. All rights reserved.
//

#import <UIKit/UIKit.h>

#define DELEGATE ((AppDelegate*)[[UIApplication sharedApplication]delegate])

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

